# Instance template variables
variable "envname" {
  description = "This value will be added to the tags for the instance, as well as the 'envname' label in the instance metadata"
  type = "string"
}

variable "service" {
  description = "This value will be added to the tags for the instance, as well as the 'service' label in the instance metadata"
  type = "string"
}

variable "domain" {
  description = "This value will be the 'domain' label in the instance metadata"
  type = "string"
}

variable "fw_tags" {
  description = "Comma seperated list of additional networking tags to add to instances built from this template"
  type = "string"
}

variable "needs_nat" {
  description = "Will add 'nat' to the list of networking tags for instances built from this template"
  type = "string"
  default = "nat"
}

variable "machine_type" {
  description = "The GCP machine type to use for instances built from this template"
  type = "string"
  default = "g1-small"
}

variable "disk_device_name" { 
  description = "The root device name of the OS disk for machines built from this template"
  type = "string"
  default = "sda"
}

variable "disk_image" {
  description = "The name of the OS image to use for machines built from this template"
  type = "string"
}

variable "net_name" {
  description = "The name of the network that machines built from this template should be connected to"
  type = "string"
}

variable "scopes" {
  description = "Comma seperated list of security scopes that should be applied to machines built from this template"
  type = "string"
  default = "storage-ro,compute-rw"
}

variable "startup_script" {
  description = "The startup script to use for instances built from this template"
  type = "string"
}

variable "ip_forward" {
  description = "Bool indicating whether the machine should allow IP forwarding, required for NAT"
  type = "string"
  default = true
}
